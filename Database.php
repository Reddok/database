<?php

class Database {

    private $_connection;
    protected $tableName;

    const DB_CREATE = 1;
    const DB_UPDATE = 2;

    public function __construct($dbType, $dbHost, $dbPort, $dbName, $dbUser, $dbPass) {
        $this->_connection = new PDO($dbType . ':host=' . $dbHost . ';port=' . $dbPort . ';dbname=' . $dbName, $dbUser, $dbPass,
            [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
    }

    public function setTable($tableName) {
        $this->checkTable($tableName);
        $this->tableName = $tableName;
        return true;
    }

    public function getTable() {
        return $this->tableName;
    }

    public function findAll($getType = PDO::FETCH_ASSOC) {
        return $this->execute('SELECT * FROM ' . $this->tableName, [], $getType);
    }

    public function findOneBy($condition, $value, $getType = PDO::FETCH_ASSOC) {
        return $this->execute('SELECT * FROM ' . $this->tableName . ' WHERE ' . $condition . ' = ? LIMIT 1;', [$value], $getType);
    }

    public function save($data, $id=null) {
        $mode = $id? static::DB_UPDATE : static::DB_CREATE;

        $columns = array_keys($data);
        $values = array_values($data);

        print_r($values);

        if($mode === static::DB_CREATE) {

            $q = 'INSERT INTO ' . $this->tableName . '(' . join(',', $columns ) . ') VALUES(' . rtrim( str_repeat('?,', count($values)), ',' ) . ');';
            print $q;
        } else {

            $q = 'UPDATE ' . $this->tableName . ' SET';

            $q .= array_reduce($columns, function($q, $column) {
                return  $q . ' ' . $column . ' = ?,';
            }, '');

            $q[strlen($q) - 1] = ' ';
            $q .= ' WHERE id = ?';
            $values[] = $id;

        }

        $q .= ';';
        return $this->execute($q, $values);


    }

    public function count($condition=null, $value=null) {
        $q = 'SELECT COUNT(id) as count FROM ' . $this->tableName;
        $values=[];

        if( isset($condition) && isset($value)) {
            $q .= ' WHERE ' . $condition . ' = ?';
            $values[] = $value;
        }

        $q .= ';';

        $result = $this->execute($q, $values);
        return $result[0]['count'];
    }

    public function delete($condition, $value) {
        return $this->execute('DELETE FROM ' . $this->tableName . ' WHERE ' . $condition . ' = ?;', [$value]);
    }

    public function complexQuery($sql,  $parameters=[], $getType=PDO::FETCH_ASSOC, $getClass=null) {
        return $this->execute($sql,  $parameters, $getType, $getClass);
    }

    protected function execute($sql, $parameters=[], $getType=PDO::FETCH_ASSOC, $getClass=null) {

        if($this->tableName) {

            $stmt = $this->_connection->prepare($sql);
            $isSelect = preg_match('/^SELECT/', $sql);

            foreach($parameters as $pos => $value) {
                $stmt->bindValue($pos + 1, $value);
            }

            $stmt->execute();

            if($isSelect) {
                return $getType === PDO::FETCH_CLASS? $stmt->fetchAll($getType, $getClass) : $stmt->fetchAll($getType);
            } else {
                return true;
            }

        } else {
            throw new PDOException('Table not selected!');
        }

    }

    protected function checkTable($tableName) {
        $this->_connection->query('SELECT 1 FROM ' . $tableName . ' LIMIT 1');
    }

}
